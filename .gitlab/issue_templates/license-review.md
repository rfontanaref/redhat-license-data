#License Review Request

## License name

(required)

## Link to text of license

(required)

## Package name and link

(required - name of package where you found this license)

##Is this license on the SPDX License List?

(required - if yes, provide link to SPDX license)

## Reason for license review

(optional - link to package review ticket)
